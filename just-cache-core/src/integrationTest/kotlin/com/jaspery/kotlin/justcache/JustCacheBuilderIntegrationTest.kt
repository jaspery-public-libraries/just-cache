package com.jaspery.kotlin.justcache

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.testng.annotations.Test

class JustCacheBuilderIntegrationTest {
    private val cache = JustCacheBuilder.build<String, Int> {
        loadType(JustCacheBuilder.CacheLoadType.ReadThrough { it.toInt() })
        replacementPolicy(CacheConfig.ReplacementPolicy.LRU)
    }

    @Test
    fun testBuilder() {
        assertThat(cache["1"]).isEqualTo(1)

        assertThatThrownBy { cache["a"] }.isInstanceOf(NumberFormatException::class.java)
    }
}
