package com.jaspery.kotlin.justcache

import com.jaspery.kotlin.justcache.CacheConfig.*
import com.jaspery.kotlin.justcache.JustCacheBuilder.CacheLoadType
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.testng.ITest
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Factory
import org.testng.annotations.Test
import java.lang.reflect.Method
import java.util.concurrent.atomic.AtomicInteger

class CacheEvictionIntegrationTest
@Factory(dataProvider = "cacheConfigDataProvider", dataProviderClass = TestDataProvider::class)
constructor(
        private val scenarioName: String, evictionType: EvictionType, replacementPolicy: ReplacementPolicy, cacheCapacity: CacheCapacity
) : ITest {
    class TestDataProvider {
        @DataProvider
        fun cacheConfigDataProvider() = dataProvider {
            scenario("noEvictionFifo", EvictionType.NoEviction, ReplacementPolicy.FIFO, CacheCapacity.Unlimited)
            scenario("noEvictionLru ", EvictionType.NoEviction, ReplacementPolicy.LRU, CacheCapacity.Unlimited)
            scenario("evictOneFifo  ", EvictionType.EvictOne, ReplacementPolicy.FIFO, CacheCapacity.Capped(32))
            scenario("evictOneLru   ", EvictionType.EvictOne, ReplacementPolicy.LRU, CacheCapacity.Capped(32))
            scenario("evictBatchFifo", EvictionType.EvictBatch(4), ReplacementPolicy.FIFO, CacheCapacity.Capped(32))
            scenario("evictBatchLru ", EvictionType.EvictBatch(4), ReplacementPolicy.LRU, CacheCapacity.Capped(32))
        }.testNGDataArray()
    }

    private val cache: Cache<String, Int>
    private val loaderInvocations = AtomicInteger()
    private val cacheSizesHistory = mutableSetOf<Int>()
    private var testName = "default test name"

    private val loader: (String) -> Int = {
        loaderInvocations.incrementAndGet()
        it.toInt()
    }

    init {
        cache = JustCacheBuilder.build {
            evictionType(evictionType)
            replacementPolicy(replacementPolicy)
            capacity(cacheCapacity)
            loadType(CacheLoadType.ReadThrough(loader))
        }
    }

    override fun getTestName(): String = testName

    @BeforeMethod
    fun setTestName(method: Method) {
        testName = "${method.name}(${scenarioName.trim()})"
    }

    @BeforeMethod
    fun resetCounters() {
        loaderInvocations.set(0)
        cacheSizesHistory.clear()
    }

    @Test
    fun startFilling() {
        (1..128).forEach {
            cache[it.toString()]
            cacheSizesHistory.add(cache.size)
        }

        assertThat(cache.size).apply {
            val evictionType = cache.config.evictionType
            val maxCapacity = cache.config.capacity.maxCapacity
            when (evictionType) {
                is EvictionType.NoEviction -> isEqualTo(128)
                is EvictionType.EvictOne -> isEqualTo(maxCapacity)
                is EvictionType.EvictBatch -> isBetween(maxCapacity, maxCapacity + evictionType.batchSize)
            }
        }
        assertThat(loaderInvocations.get()).isEqualTo(128)
    }

    @Test(dependsOnMethods = ["startFilling"])
    fun testRetrieval() {
        (1..128).forEach {
            cache[it.toString()]
            cacheSizesHistory.add(cache.size)
        }
        assertThat(cache.size).apply {
            val evictionType = cache.config.evictionType
            val maxCapacity = cache.config.capacity.maxCapacity
            when (evictionType) {
                is EvictionType.NoEviction -> isEqualTo(128)
                is EvictionType.EvictOne -> isEqualTo(maxCapacity)
                is EvictionType.EvictBatch -> isBetween(maxCapacity, maxCapacity + evictionType.batchSize)
            }
        }
        assertThat(loaderInvocations.get()).isEqualTo(if (cache.config.evictionType is EvictionType.NoEviction) 0 else 128)
    }

    @Test(dependsOnMethods = ["testRetrieval"])
    fun testClear() {
        cache.clear()
        assertThat(cache.size).isEqualTo(0)
    }

    @Test(dependsOnMethods = ["testClear"])
    fun testFillingAgain() {
        (1..32).forEach {
            cache[it.toString()]
            cacheSizesHistory.add(cache.size)
        }
        assertThat(cache.size).apply {
            val evictionType = cache.config.evictionType
            val maxCapacity = cache.config.capacity.maxCapacity
            when (evictionType) {
                is EvictionType.NoEviction -> isEqualTo(32)
                is EvictionType.EvictOne -> isEqualTo(maxCapacity)
                is EvictionType.EvictBatch -> isBetween(maxCapacity, maxCapacity + evictionType.batchSize)
            }
        }
        assertThat(loaderInvocations.get()).isEqualTo(32)
    }

    private val CacheCapacity.maxCapacity
        get() = when (this) {
            is CacheCapacity.Unlimited -> CacheConfig.MAXIMUM_CAPACITY; is CacheCapacity.Capped -> this.capacity
        }
}
