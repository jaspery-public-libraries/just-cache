package  com.jaspery.kotlin.justcache.serviceloader

import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig
import com.jaspery.kotlin.justcache.impl.LinkedHashMapCache

class CacheImplementationFactoryImpl : CacheImplementationFactory {
    override fun <K, V> newInstance(loader: (K) -> V, config: CacheConfig): Cache<K, V> = LinkedHashMapCache(loader, config)
}
