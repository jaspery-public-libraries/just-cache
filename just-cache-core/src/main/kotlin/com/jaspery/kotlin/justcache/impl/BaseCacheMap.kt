package com.jaspery.kotlin.justcache.impl


internal class FifoMap<K, V>(evictionStrategy: EvictionStrategy<K, V>)
    : BaseCacheMap<K, V>(evictionStrategy, false)

internal class LruMap<K, V>(evictionStrategy: EvictionStrategy<K, V>)
    : BaseCacheMap<K, V>(evictionStrategy, true)

internal abstract class BaseCacheMap<K, V>(private val evictionStrategy: EvictionStrategy<K, V>, accessOrder: Boolean) : LinkedHashMap<K, V>(
        DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, accessOrder
) {
    override fun removeEldestEntry(eldest: MutableMap.MutableEntry<K, V>?): Boolean {
        return evictionStrategy.removeEldestEntries(this, eldest)
    }

    private companion object {
        private const val DEFAULT_INITIAL_CAPACITY = 1 shl 3

        private const val DEFAULT_LOAD_FACTOR = 0.75f
    }
}
