/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.justcache.impl

import com.jaspery.kotlin.justcache.CacheConfig
import com.jaspery.kotlin.justcache.CacheConfig.CacheCapacity.Capped
import com.jaspery.kotlin.justcache.CacheConfig.CacheCapacity.Unlimited
import com.jaspery.kotlin.justcache.CacheConfig.EvictionType
import kotlin.collections.MutableMap.MutableEntry

internal sealed class EvictionStrategy<K, V> {
    internal abstract fun removeEldestEntries(target: MutableMap<K, V>, eldest: MutableEntry<K, V>?): Boolean

    internal companion object {
        internal fun <K, V> getEvictionStrategy(cacheConfig: CacheConfig): EvictionStrategy<K, V> {
            return when (val capacity = cacheConfig.capacity) {
                is Unlimited -> noEviction()
                is Capped -> when (val evictionType = cacheConfig.evictionType) {
                    is EvictionType.NoEviction -> noEviction()
                    is EvictionType.EvictOne -> EvictOneStrategy<K, V>(capacity.capacity)
                    is EvictionType.EvictBatch -> EvictBatchStrategy<K, V>(capacity.capacity, evictionType.batchSize)
                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        private fun <K, V> noEviction(): EvictionStrategy<K, V> = NoEvictionStrategy as EvictionStrategy<K, V>
    }

    internal object NoEvictionStrategy : EvictionStrategy<Nothing, Nothing>() {
        override fun removeEldestEntries(target: MutableMap<Nothing, Nothing>, eldest: MutableEntry<Nothing, Nothing>?) = false
    }

    internal class EvictOneStrategy<K, V>(private val capacity: Int) : EvictionStrategy<K, V>() {
        override fun removeEldestEntries(target: MutableMap<K, V>, eldest: MutableEntry<K, V>?): Boolean = target.size > capacity
    }

    internal class EvictBatchStrategy<K, V>(private val maxCapacity: Int, private val batchSize: Int) : EvictionStrategy<K, V>() {
        override fun removeEldestEntries(target: MutableMap<K, V>, eldest: MutableEntry<K, V>?): Boolean {
            if (target.size > maxCapacity + batchSize) {
                target.keys.removeFirst(batchSize)
            }
            return false
        }
    }

    internal fun <E> MutableSet<E>.removeFirst(n: Int) = this.removeAll(this.take(n))
}
