package com.jaspery.kotlin.justcache.impl

import com.jaspery.kotlin.justcache.CacheConfig

open class BaseLinkedHashMapCache(
        open val config: CacheConfig
) {
    protected fun <K, V> createLinkedHashMap(): MutableMap<K, V> = when (config.replacementPolicy) {
        CacheConfig.ReplacementPolicy.FIFO -> FifoMap(EvictionStrategy.getEvictionStrategy(config))
        CacheConfig.ReplacementPolicy.LRU -> LruMap(EvictionStrategy.getEvictionStrategy(config))
    }
}
