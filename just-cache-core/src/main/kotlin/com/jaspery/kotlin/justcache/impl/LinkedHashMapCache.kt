package com.jaspery.kotlin.justcache.impl

import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig

/**
 * [LinkedHashMap]-based read through LRU cache. It removes least recently used items in
 * chunks of size specified by [extraEntriesNum]. This cache stores up to
 * [maxCacheSize] + [extraEntriesNum] entries, all of them are available until
 * [size] is < [maxCacheSize] + [extraEntriesNum]. [loader] is used to fetch value
 * which is missing in cache
 *
 * @param loader function used to load value for key [K] which is missing in cache
 * @param maxCacheSize number of entries guaranteed to be maintained in cache
 * @param extraEntriesNum extra number of entries stored in cache on top of [maxCacheSize]. When
 * [size] reaches [maxCacheSize] + [extraEntriesNum], [extraEntriesNum] of least recently used
 * entries are removed at once.
 */
internal class LinkedHashMapCache<K, V>(
        private val loader: (K) -> V,
        override val config: CacheConfig
) : BaseLinkedHashMapCache(config), Cache<K, V> {
    private val entries: MutableMap<K, V> = createLinkedHashMap()

    override operator fun get(key: K): V = entries.getOrPut(key) { loader(key) }

    override val size: Int get() = entries.size

    override fun clear() = entries.clear()
}
