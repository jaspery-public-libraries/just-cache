package com.jaspery.kotlin.justcache.impl

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class RemoveFirstTest {
    private val mutableSet: MutableSet<String> = LinkedHashSet<String>().apply {
        (1..15).forEach {
            add(it.toString())
        }
    }

    @DataProvider
    fun provideMutableSet() = dataProvider { scenario(LinkedHashSet(mutableSet)) }.testNGDataArray()

    @Test(dataProvider = "provideMutableSet")
    fun removeFirstFive(set: MutableSet<String>) {
        with(EvictionStrategy.NoEvictionStrategy) {
            set.removeFirst(5)
        }
        assertThat(set).hasSize(10)
    }

    @Test(dataProvider = "provideMutableSet")
    fun removeFirstTwenty(set: MutableSet<String>) {
        with(EvictionStrategy.NoEvictionStrategy) {
            set.removeFirst(20)
        }
        assertThat(set).isEmpty()
    }

    @Test(dataProvider = "provideMutableSet")
    fun removeFirstNone(set: MutableSet<String>) {
        with(EvictionStrategy.NoEvictionStrategy) {
            set.removeFirst(0)
        }
        assertThat(set).hasSize(15)
    }

}