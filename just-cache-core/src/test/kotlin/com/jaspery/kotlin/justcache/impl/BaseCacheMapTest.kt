package com.jaspery.kotlin.justcache.impl

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test

class BaseCacheMapTest {
    private val evictionStrategy: EvictionStrategy<String, String> = mockk()


    @Test
    fun testFifo() {
        val fifoMap = FifoMap(evictionStrategy)

        every { evictionStrategy.removeEldestEntries(fifoMap, any()) } returnsMany listOf(false, false, true)

        fifoMap["first"] = "1"
        fifoMap["second"] = "2"
        fifoMap["third"] = "3"

        assertThat(fifoMap).doesNotContainKey("first")
        assertThat(fifoMap).containsKey("second")
        assertThat(fifoMap).containsKey("third")

        verify(exactly = 3) { evictionStrategy.removeEldestEntries(fifoMap, any()) }
    }

    @Test
    fun testLru() {
        val lruMap = LruMap(evictionStrategy)

        every { evictionStrategy.removeEldestEntries(lruMap, any()) } returnsMany listOf(false, false, true)

        lruMap["first"] = "1"
        lruMap["second"] = "2"
        // touch "first" to prevent it's eviction
        assertThat(lruMap["first"]).isEqualTo("1")
        lruMap["third"] = "3"


        assertThat(lruMap).containsKey("first")
        assertThat(lruMap).doesNotContainKey("second")
        assertThat(lruMap).containsKey("third")

        verify(exactly = 3) { evictionStrategy.removeEldestEntries(lruMap, any()) }
    }

}