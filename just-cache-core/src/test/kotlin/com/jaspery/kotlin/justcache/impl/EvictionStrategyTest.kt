/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.justcache.impl

import com.jaspery.kotlin.justcache.CacheConfig
import com.jaspery.kotlin.justcache.CacheConfig.CacheCapacity.Capped
import com.jaspery.kotlin.justcache.CacheConfig.CacheCapacity.Unlimited
import com.jaspery.kotlin.justcache.CacheConfig.EvictionType.*
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import kotlin.reflect.KClass

internal class EvictionStrategyTest {
    @Test(dataProvider = "getEvictionStrategyDataProvider")
    fun testGetEvictionStrategy(cacheConfig: CacheConfig, evictionStrategyClass: KClass<EvictionStrategy<*, *>>) {
        assertThat(
                EvictionStrategy.getEvictionStrategy<String, Int>(cacheConfig)
        ).isInstanceOf(evictionStrategyClass.java)
    }

    @DataProvider
    fun getEvictionStrategyDataProvider() = dataProvider {
        scenario(CacheConfig(capacity = Unlimited, evictionType = NoEviction), EvictionStrategy.NoEvictionStrategy::class)
        scenario(CacheConfig(capacity = Unlimited, evictionType = EvictOne), EvictionStrategy.NoEvictionStrategy::class)
        scenario(CacheConfig(capacity = Unlimited, evictionType = EvictBatch(5)), EvictionStrategy.NoEvictionStrategy::class)

        scenario(CacheConfig(capacity = Capped(150), evictionType = NoEviction), EvictionStrategy.NoEvictionStrategy::class)
        scenario(CacheConfig(capacity = Capped(150), evictionType = EvictOne), EvictionStrategy.EvictOneStrategy::class)
        scenario(CacheConfig(capacity = Capped(150), evictionType = EvictBatch(5)), EvictionStrategy.EvictBatchStrategy::class)
    }.testNGDataArray()

    class NoEvictionStrategyTest {
        @Test
        fun test() {
            assertThat(EvictionStrategy.NoEvictionStrategy.removeEldestEntries(mockk(), mockk())).isFalse()
        }
    }

    class EvictOneStrategyTest {
        private val target: MutableMap<Any, Any> = mockk()

        @BeforeMethod
        fun resetMocks() = clearMocks(target)

        @Test(dataProvider = "evictOneStrategyDataProvider")
        fun test(maxCapacity: Int, currentMapSize: Int, evict: Boolean) {
            every { target.size } returns currentMapSize

            val strategy = EvictionStrategy.EvictOneStrategy<Any, Any>(maxCapacity)
            val result = strategy.removeEldestEntries(target, mockk())
            assertThat(result).isEqualTo(evict)

            verify(exactly = 1) { target.size }
        }

        @DataProvider
        fun evictOneStrategyDataProvider() = dataProvider {
            scenario(150, 15, false)
            scenario(150, 150, false)
            scenario(150, 151, true)
        }.testNGDataArray()
    }

    class EvictBatchStrategyTest {
        private val target: MutableMap<Any, Any> = mockk()
        private val targetKeys: MutableSet<Any> = mockk()

        @BeforeMethod
        fun resetMocks() = clearMocks(target, targetKeys)

        @Test(dataProvider = "evictBatchStrategyDataProvider")
        fun test(maxCapacity: Int, batchSize: Int, currentMapSize: Int, evict: Boolean) {
            every { target.size } returns currentMapSize
            every { target.keys } returns targetKeys

            val strategy = spyk(EvictionStrategy.EvictBatchStrategy<Any, Any>(maxCapacity, batchSize)) {
                every { targetKeys.removeFirst(batchSize) } returns true
            }

            val result = strategy.removeEldestEntries(target, mockk())
            assertThat(result).isEqualTo(false)

            verify(exactly = 1) { target.size }
            with(strategy) {
                verify(exactly = if (evict) 1 else 0) { targetKeys.removeFirst(batchSize) }
            }
        }

        @DataProvider
        fun evictBatchStrategyDataProvider() = dataProvider {
            scenario(150, 15, 166, true)
            scenario(150, 15, 165, false)
            scenario(150, 15, 164, false)
        }.testNGDataArray()
    }
}