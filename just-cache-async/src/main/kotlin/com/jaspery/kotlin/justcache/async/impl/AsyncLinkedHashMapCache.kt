/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 *
 */

package com.jaspery.kotlin.justcache.async.impl

import com.jaspery.kotlin.justcache.AsyncCache
import com.jaspery.kotlin.justcache.AsyncCacheConfig
import com.jaspery.kotlin.justcache.CacheConfig
import com.jaspery.kotlin.justcache.impl.BaseLinkedHashMapCache
import kotlinx.coroutines.*
import kotlinx.coroutines.CoroutineStart.LAZY
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.coroutines.CoroutineContext

/**
 * [LinkedHashMap]-based read through LRU cache. It removes least recently used items in
 * chunks of size specified by [extraEntriesNum]. This cache stores up to
 * [maxCacheSize] + [extraEntriesNum] entries, all of them are available until
 * [size] is < [maxCacheSize] + [extraEntriesNum]. [loader] is used to fetch value
 * which is missing in cache
 *
 * @param loader function used to load value for key [K] which is missing in cache
 * @param maxCacheSize number of entries guaranteed to be maintained in cache
 * @param extraEntriesNum extra number of entries stored in cache on top of [maxCacheSize]. When
 * [size] reaches [maxCacheSize] + [extraEntriesNum], [extraEntriesNum] of least recently used
 * entries are removed at once.
 */
internal class AsyncLinkedHashMapCache<K, V>(
        private val loader: (K) -> V,
        override val config: CacheConfig,
        override val asyncCacheConfig: AsyncCacheConfig
) : BaseLinkedHashMapCache(config), AsyncCache<K, V>, CoroutineScope {
    private val parentJob = SupervisorJob()
    private val structureMutex = Mutex()
    private val entries: MutableMap<K, Deferred<V>> = createLinkedHashMap()

    override val coroutineContext: CoroutineContext
        get() = asyncCacheConfig.coroutineContext + parentJob

    override suspend fun get(key: K): V {
        while (parentJob.isActive) {
            val d: Deferred<V> = structureMutex.withLock {
                entries[key] ?: run {
                    val dt: Deferred<V> = async(start = LAZY) { loader(key) }
                    entries.putIfAbsent(key, dt)
                            ?: run {
                                dt.start()
                                dt
                            }
                }
            }

            try {
                return d.await()
            } catch (e: CancellationException) {
                @Suppress("DeferredResultUnused")
                structureMutex.withLock { entries.remove(key) }
            } catch (e: Exception) {
                println(e)
                throw e
            }
        }
        throw IllegalStateException("Cache is inactive")
    }

    override suspend fun clear() = structureMutex.withLock { entries.clear() }

    override fun close() {
        parentJob.cancel()
        launch { clear() }
        runBlocking { clear() }
    }
}
