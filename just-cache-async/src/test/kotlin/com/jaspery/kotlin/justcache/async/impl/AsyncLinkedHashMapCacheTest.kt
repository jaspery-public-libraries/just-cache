/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 *
 */

package com.jaspery.kotlin.justcache.async.impl

import com.jaspery.kotlin.justcache.AsyncCacheConfig
import com.jaspery.kotlin.justcache.CacheConfig
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.testng.annotations.Test

typealias LoaderFunction<K, V> = (K) -> V

class AsyncLinkedHashMapCacheTest {
    private val cacheConfig: CacheConfig = CacheConfig(
            CacheConfig.CacheCapacity.Capped(5),
            CacheConfig.ReplacementPolicy.LRU,
            CacheConfig.EvictionType.EvictOne
    )
    private val asyncCacheConfig: AsyncCacheConfig = AsyncCacheConfig(
            Dispatchers.Default
    )
    private val loader: LoaderFunction<String, Int> = mockk()

    private val cache = AsyncLinkedHashMapCache<String, Int>(
            loader,
            cacheConfig,
            asyncCacheConfig
    )

    @Test
    fun test() = runBlocking {
        every { loader(any()) } returns 1

        cache.get("1")
        cache.get("1")
        cache.get("1")
        cache.get("1")
        cache.get("1")
        cache.get("1")

        verify(exactly = 1) { loader(any()) }
    }
}