dependencies {
    api(project(":just-cache-api"))
    implementation(project(":just-cache-core"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
}