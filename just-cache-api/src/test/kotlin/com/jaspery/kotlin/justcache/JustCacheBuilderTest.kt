package com.jaspery.kotlin.justcache

import com.jaspery.kotlin.justcache.CacheConfig.*
import com.jaspery.kotlin.justcache.JustCacheBuilder.CacheLoadType.ReadThrough
import com.jaspery.kotlin.justcache.serviceloader.AsyncCacheImplementationFactory
import com.jaspery.kotlin.justcache.serviceloader.CacheImplementationFactory
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import de.jupf.staticlog.Log
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import kotlin.coroutines.CoroutineContext

typealias LoaderFunction<K, V> = (K) -> V

@Test
class JustCacheBuilderTest {
    @Test
    fun testBuildCacheDefaults() {
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.build<String, Int> {}
        }.withMessageStartingWith("JustCache only support ReadThrough load strategy.")
    }

    @Test
    fun testBuildCacheAsyncWithoutCoroutineContext() {
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.buildAsync<String, Int> {}
        }.withMessageStartingWith("buildAsync() requires coroutine context to be specified")
    }

    @Test(dataProvider = "unsupportedCapacityValues")
    fun testCapacityNotSupported(requestedCapacity: Int) {
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.build<String, Int> {
                capacity(CacheCapacity.Capped(requestedCapacity))
            }
        }.withMessage("Requested capacity ($requestedCapacity) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")

        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.buildAsync<String, Int> {
                capacity(CacheCapacity.Capped(requestedCapacity))
            }
        }.withMessage("Requested capacity ($requestedCapacity) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")
    }

    @Test(dataProvider = "unsupportedCapacityValues")
    fun testBatchSizeNotSupported(requestedBatchSize: Int) {
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.build<String, Int> {
                evictionType(EvictionType.EvictBatch(requestedBatchSize))
            }
        }.withMessage("Requested batchSize ($requestedBatchSize) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")

        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.buildAsync<String, Int> {
                evictionType(EvictionType.EvictBatch(requestedBatchSize))
            }
        }.withMessage("Requested batchSize ($requestedBatchSize) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")
    }

    @DataProvider
    fun unsupportedCapacityValues() = dataProvider {
        scenario(-1)
        scenario(0)
        scenario(Int.MAX_VALUE)
    }.testNGDataArray()

    @Test(dataProvider = "unsupportedCapacityPlusBatchSizeValues")
    fun testCapactiyPlusBatchSizeUnsupported(capacity: Int, batchSize: Int) {
        Log.info("capacity: $capacity, batchSize: $batchSize")
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.build<String, Int> {
                loadType(ReadThrough(String::toInt))
                capacity(CacheCapacity.Capped(capacity))
                evictionType(EvictionType.EvictBatch(batchSize))
            }
        }.withMessageStartingWith("Cache capacity (capacity + eviction batch size) cannot exceed")
    }

    @DataProvider
    fun unsupportedCapacityPlusBatchSizeValues() = dataProvider {
        scenario(1 shl 30, 5)
        scenario(1 shl 30, 2)
        scenario(1 shl 29 + 1, 1 shl 29)
    }.testNGDataArray()

    @Test
    fun testCapactiyPlusEvictOneUnsupported() {
        assertThatIllegalArgumentException().isThrownBy {
            JustCacheBuilder.build<String, Int> {
                loadType(ReadThrough(String::toInt))
                capacity(CacheCapacity.Capped(CacheConfig.MAXIMUM_CAPACITY))
                evictionType(EvictionType.EvictOne)
            }
        }.withMessageStartingWith("Cache capacity (capacity + eviction batch size) cannot exceed")
    }


    @Test
    fun testBuildCacheReadThroughDefaults() {
        val cacheConfigSlot = slot<CacheConfig>()
        mockkObject(CacheImplementationFactory.Companion)

        every {
            CacheImplementationFactory.load(any()).newInstance(
                    captureLambda<LoaderFunction<String, Int>>(),
                    capture(cacheConfigSlot))
        } returns mockk()

        JustCacheBuilder.build<String, Int> {
            loadType(ReadThrough { it -> it.toInt() })
        }

        assertThat(cacheConfigSlot.captured.capacity).isEqualTo(CacheCapacity.Unlimited)
        assertThat(cacheConfigSlot.captured.evictionType).isEqualTo(EvictionType.NoEviction)
        assertThat(cacheConfigSlot.captured.replacementPolicy).isEqualTo(ReplacementPolicy.FIFO)

        verify(exactly = 1) { CacheImplementationFactory.load(any()).newInstance(any<LoaderFunction<String, Int>>(), any()) }

        unmockkObject(CacheImplementationFactory.Companion)
    }

    @Test
    fun testBuildCacheHappyPath() {
        val cacheConfigSlot = slot<CacheConfig>()
        mockkObject(CacheImplementationFactory.Companion)

        every {
            CacheImplementationFactory.load(any()).newInstance(
                    captureLambda<LoaderFunction<String, Int>>(),
                    capture(cacheConfigSlot))
        } returns mockk()

        JustCacheBuilder.build<String, Int> {
            loadType(ReadThrough { it -> it.toInt() })
            capacity(CacheCapacity.Capped(100))
            evictionType(EvictionType.EvictBatch(5))
            replacementPolicy(ReplacementPolicy.LRU)
        }

        assertThat(cacheConfigSlot.captured.capacity).isEqualTo(CacheCapacity.Capped(100))
        assertThat(cacheConfigSlot.captured.evictionType).isEqualTo(EvictionType.EvictBatch(5))
        assertThat(cacheConfigSlot.captured.replacementPolicy).isEqualTo(ReplacementPolicy.LRU)

        verify(exactly = 1) { CacheImplementationFactory.load(any()).newInstance(any<LoaderFunction<String, Int>>(), any()) }

        unmockkObject(CacheImplementationFactory.Companion)
    }

    @Test
    fun testBuildCacheHappyPathAsync() {
        val coroutineContext: CoroutineContext = mockk()

        val cacheConfigSlot = slot<CacheConfig>()
        val asyncCacheConfigSlot = slot<AsyncCacheConfig>()

        mockkObject(AsyncCacheImplementationFactory.Companion)

        every {
            AsyncCacheImplementationFactory.load(any()).newInstance(
                    captureLambda<LoaderFunction<String, Int>>(),
                    capture(cacheConfigSlot),
                    capture(asyncCacheConfigSlot))
        } returns mockk()

        JustCacheBuilder.buildAsync<String, Int> {
            coroutineContext(coroutineContext)
            loadType(ReadThrough { it -> it.toInt() })
            capacity(CacheCapacity.Capped(100))
            evictionType(EvictionType.EvictOne)
            replacementPolicy(ReplacementPolicy.LRU)
        }

        assertThat(cacheConfigSlot.captured.capacity).isEqualTo(CacheCapacity.Capped(100))
        assertThat(cacheConfigSlot.captured.evictionType).isEqualTo(EvictionType.EvictOne)
        assertThat(cacheConfigSlot.captured.replacementPolicy).isEqualTo(ReplacementPolicy.LRU)
        assertThat(asyncCacheConfigSlot.captured.coroutineContext).isEqualTo(coroutineContext)

        verify(exactly = 1) { AsyncCacheImplementationFactory.load(any()).newInstance(any<LoaderFunction<String, Int>>(), any(), any()) }

        unmockkObject(AsyncCacheImplementationFactory.Companion)
    }
}
