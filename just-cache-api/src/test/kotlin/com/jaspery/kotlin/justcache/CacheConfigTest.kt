package com.jaspery.kotlin.justcache

import com.jaspery.kotlin.justcache.CacheConfig.*
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class CacheConfigTest {
    @DataProvider
    fun illegalCapacityDataProvider() = dataProvider {
        scenario(-128)
        scenario(-1)
        scenario(0)
        scenario(CacheConfig.MAXIMUM_CAPACITY + 1)
    }.testNGDataArray()

    @Test(dataProvider = "illegalCapacityDataProvider")
    fun testCacheCapacityConstraints(illegalCapacity: Int) {
        assertThatIllegalArgumentException().isThrownBy {
            CacheCapacity.Capped(illegalCapacity)
        }.withMessageMatching("Requested capacity \\(-?\\d+\\) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")
    }

    @Test(dataProvider = "illegalCapacityDataProvider")
    fun testEvictionBatchSizeConstraints(illegalCapacity: Int) {
        assertThatIllegalArgumentException().isThrownBy {
            EvictionType.EvictBatch(illegalCapacity)
        }.withMessageMatching("Requested batchSize \\(-?\\d+\\) cannot be higher than ${CacheConfig.MAXIMUM_CAPACITY}")
    }

    @Test
    fun testEvictionTypeToStrings() {
        assertThat(EvictionType.NoEviction).hasToString("EvictionType.NoEviction")
        assertThat(EvictionType.EvictOne).hasToString("EvictionType.EvictOne")
        assertThat(EvictionType.EvictBatch(5)).hasToString("EvictBatch(batchSize=5)")
    }

    @Test
    fun testReplacementPolicyToString() {
        assertThat(ReplacementPolicy.FIFO).hasToString("FIFO")
        assertThat(ReplacementPolicy.LRU).hasToString("LRU")
    }

    @Test
    fun testCacheCapacityToString() {
        assertThat(CacheCapacity.Unlimited).hasToString("CacheCapacity.Unlimited")
        assertThat(CacheCapacity.Capped(32)).hasToString("Capped(capacity=32)")
    }
}