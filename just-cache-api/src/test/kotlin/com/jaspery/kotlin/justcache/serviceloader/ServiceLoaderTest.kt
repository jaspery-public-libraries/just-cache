package com.jaspery.kotlin.justcache.serviceloader

import com.jaspery.kotlin.justcache.AsyncCache
import com.jaspery.kotlin.justcache.AsyncCacheConfig
import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig
import io.mockk.*
import org.assertj.core.api.Assertions.assertThatIllegalStateException
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class ServiceLoaderTest {
    private val classLoader: ClassLoader = mockk()

    private companion object {
        private const val ServiceLoaderKtFile = "com.jaspery.kotlin.justcache.serviceloader.ServiceLoaderKt"
        private val ServiceLoaderKtClass = Class.forName(ServiceLoaderKtFile).kotlin
    }

    @BeforeMethod
    fun resetMocks() {
        clearMocks(classLoader)
        clearStaticMockk(ServiceLoaderKtClass)
    }

    @BeforeClass
    fun mockServiceLoaderKt() = mockkStatic(ServiceLoaderKtClass)

    @AfterClass
    fun unmockServiceLoaderKt() = unmockkStatic(ServiceLoaderKtClass)


    @Test
    fun `test load CacheImplementationFactory happy`() {
        every { serviceLoaderLoad(CacheImplementationFactory::class, classLoader) } returns listOf(CacheImplementationFactoryImpl)

        CacheImplementationFactory.load(classLoader)

        verify(exactly = 1) { serviceLoaderLoad(CacheImplementationFactory::class, classLoader) }
    }

    @Test
    fun `test load AsyncCacheImplementationFactory happy`() {
        every { serviceLoaderLoad(AsyncCacheImplementationFactory::class, classLoader) } returns listOf(AsyncCacheImplementationFactoryImpl)

        AsyncCacheImplementationFactory.load(classLoader)

        verify(exactly = 1) { serviceLoaderLoad(AsyncCacheImplementationFactory::class, classLoader) }
    }

    @Test
    fun `test load CacheImplementationFactory unhappy`() {
        every { serviceLoaderLoad(CacheImplementationFactory::class, classLoader) } returns emptyList()

        assertThatIllegalStateException().isThrownBy {
            CacheImplementationFactory.load(classLoader)
        }.withMessageStartingWith(CacheImplementationFactory.PROVIDER_MISSING_MSG)

        verify(exactly = 1) { serviceLoaderLoad(CacheImplementationFactory::class, classLoader) }
    }

    @Test
    fun `test load AsyncCacheImplementationFactory unhappy`() {
        every { serviceLoaderLoad(AsyncCacheImplementationFactory::class, classLoader) } returns emptyList()

        assertThatIllegalStateException().isThrownBy {
            AsyncCacheImplementationFactory.load(classLoader)
        }.withMessageStartingWith(AsyncCacheImplementationFactory.PROVIDER_MISSING_MSG)

        verify(exactly = 1) { serviceLoaderLoad(AsyncCacheImplementationFactory::class, classLoader) }
    }

    object CacheImplementationFactoryImpl : CacheImplementationFactory {
        override fun <K, V> newInstance(loader: (K) -> V, config: CacheConfig): Cache<K, V> = throw NotImplementedError()
    }

    object AsyncCacheImplementationFactoryImpl : AsyncCacheImplementationFactory {
        override fun <K, V> newInstance(loader: (K) -> V, config: CacheConfig, asyncConfig: AsyncCacheConfig): AsyncCache<K, V> = throw NotImplementedError()
    }
}