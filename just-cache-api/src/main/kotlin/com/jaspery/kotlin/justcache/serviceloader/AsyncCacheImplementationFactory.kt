package com.jaspery.kotlin.justcache.serviceloader

import com.jaspery.kotlin.justcache.AsyncCache
import com.jaspery.kotlin.justcache.AsyncCacheConfig
import com.jaspery.kotlin.justcache.CacheConfig

internal interface AsyncCacheImplementationFactory {
    fun <K, V> newInstance(loader: (K) -> V, config: CacheConfig, asyncConfig: AsyncCacheConfig): AsyncCache<K, V>

    companion object {
        internal fun load(cl: ClassLoader): AsyncCacheImplementationFactory =
                serviceLoaderLoad(AsyncCacheImplementationFactory::class, cl).firstOrNull()
                        ?: throw IllegalStateException(PROVIDER_MISSING_MSG)

        internal const val PROVIDER_MISSING_MSG = "AsyncCacheImplementationFactory provider not found. " +
                "You may need to add just-cache-async to the classpath"
    }
}
