package com.jaspery.kotlin.justcache

import kotlin.coroutines.CoroutineContext

data class AsyncCacheConfig(
        val coroutineContext: CoroutineContext
)
