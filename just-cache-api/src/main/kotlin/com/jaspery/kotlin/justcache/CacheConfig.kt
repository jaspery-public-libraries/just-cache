package com.jaspery.kotlin.justcache

import com.jaspery.kotlin.lang.requireValueIn

data class CacheConfig(
        val capacity: CacheCapacity = CacheCapacity.Unlimited,
        val replacementPolicy: ReplacementPolicy = ReplacementPolicy.FIFO,
        val evictionType: EvictionType = EvictionType.NoEviction
) {
    enum class ReplacementPolicy {
        FIFO, LRU
    }

    sealed class EvictionType {
        object NoEviction : EvictionType() {
            override fun toString(): String = "EvictionType.NoEviction"
        }

        object EvictOne : EvictionType() {
            override fun toString(): String = "EvictionType.EvictOne"
        }

        data class EvictBatch(val batchSize: Int) : EvictionType() {
            init {
                requireValueIn(batchSize, 1..MAXIMUM_CAPACITY) {
                    "Requested batchSize ($batchSize) cannot be higher than $MAXIMUM_CAPACITY"
                }
            }
        }
    }

    sealed class CacheCapacity {
        object Unlimited : CacheCapacity() {
            override fun toString(): String = "CacheCapacity.Unlimited"
        }

        data class Capped(val capacity: Int) : CacheCapacity() {
            init {
                requireValueIn(capacity, 1..MAXIMUM_CAPACITY) {
                    "Requested capacity ($capacity) cannot be higher than $MAXIMUM_CAPACITY"
                }
            }
        }
    }

    companion object {
        const val MAXIMUM_CAPACITY = 1 shl 30
    }
}
