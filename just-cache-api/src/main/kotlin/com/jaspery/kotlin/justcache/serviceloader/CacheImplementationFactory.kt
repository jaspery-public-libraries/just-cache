package com.jaspery.kotlin.justcache.serviceloader

import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig

interface CacheImplementationFactory {
    fun <K, V> newInstance(loader: (K) -> V, config: CacheConfig): Cache<K, V>

    companion object {
        internal fun load(cl: ClassLoader): CacheImplementationFactory =
                serviceLoaderLoad(CacheImplementationFactory::class, cl).firstOrNull()
                        ?: throw IllegalStateException(PROVIDER_MISSING_MSG)

        internal const val PROVIDER_MISSING_MSG = "CacheImplementationFactory provider not found. " +
                "You may need to add just-cache-core to the classpath"
    }
}

