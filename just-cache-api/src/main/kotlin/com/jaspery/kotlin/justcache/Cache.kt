package com.jaspery.kotlin.justcache

interface Cache<K, V> {
    operator fun get(key: K): V

    val size: Int

    fun clear()

    val config: CacheConfig
}
