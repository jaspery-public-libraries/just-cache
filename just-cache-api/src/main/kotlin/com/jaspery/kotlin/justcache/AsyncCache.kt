package com.jaspery.kotlin.justcache

interface AsyncCache<K, V> {
    suspend fun get(key: K): V

    suspend fun clear()

    fun close()

    val config: CacheConfig

    val asyncCacheConfig: AsyncCacheConfig
}
