package com.jaspery.kotlin.justcache

import com.jaspery.kotlin.justcache.CacheConfig.*
import com.jaspery.kotlin.justcache.JustCacheBuilder.CacheLoadType.ReadThrough
import com.jaspery.kotlin.justcache.serviceloader.AsyncCacheImplementationFactory
import com.jaspery.kotlin.justcache.serviceloader.CacheImplementationFactory
import kotlin.coroutines.CoroutineContext

class JustCacheBuilder<K, V> {
    private var capacity: CacheCapacity = CacheCapacity.Unlimited
    private var replacementPolicy: ReplacementPolicy = ReplacementPolicy.FIFO
    private var evictionType: EvictionType = EvictionType.NoEviction
    private var loadType: CacheLoadType<K, V> = CacheLoadType.CacheAside
    private var coroutineContext: CoroutineContext? = null

    fun capacity(capacity: CacheCapacity) = apply { this.capacity = capacity }
    fun replacementPolicy(policy: ReplacementPolicy) = apply { this.replacementPolicy = policy }
    fun evictionType(evictionType: EvictionType) = apply { this.evictionType = evictionType }
    fun loadType(loadType: CacheLoadType<K, V>) = apply { this.loadType = loadType }
    fun coroutineContext(coroutineContext: CoroutineContext) = apply { this.coroutineContext = coroutineContext }

    fun build(): Cache<K, V> {
        val loadType = this.loadType as? ReadThrough<K, V> ?: reatThroughRequired()
        val cacheConfig = validateMaxCapacity(CacheConfig(capacity, replacementPolicy, evictionType))
        val factory = CacheImplementationFactory.load(this::class.java.classLoader)
        return factory.newInstance(loadType.loader, cacheConfig)
    }

    fun buildAsync(): AsyncCache<K, V> {
        val coroutineContext = requireNotNull(this.coroutineContext) {
            "buildAsync() requires coroutine context to be specified"
        }
        val loadType = this.loadType as? ReadThrough<K, V> ?: reatThroughRequired()
        val cacheConfig = validateMaxCapacity(CacheConfig(capacity, replacementPolicy, evictionType))
        val factory = AsyncCacheImplementationFactory.load(this::class.java.classLoader)
        return factory.newInstance(loadType.loader, cacheConfig, AsyncCacheConfig(coroutineContext)
        )
    }

    private fun reatThroughRequired(): Nothing {
        throw IllegalArgumentException("JustCache only support ReadThrough load strategy. Please, specify it explicitly.")
    }

    private fun validateMaxCapacity(cacheConfig: CacheConfig): CacheConfig = cacheConfig.apply {
        when (capacity) {
            is CacheCapacity.Unlimited -> {
            }
            is CacheCapacity.Capped -> when (evictionType) {
                is EvictionType.NoEviction -> {
                }
                is EvictionType.EvictOne -> require(capacity.capacity + 1 <= CacheConfig.MAXIMUM_CAPACITY) {
                    "Cache capacity (capacity + eviction batch size) cannot exceed ${CacheConfig.MAXIMUM_CAPACITY}"
                }
                is EvictionType.EvictBatch -> require(capacity.capacity + evictionType.batchSize <= CacheConfig.MAXIMUM_CAPACITY) {
                    "Cache capacity (capacity + eviction batch size) cannot exceed ${CacheConfig.MAXIMUM_CAPACITY}"
                }
            }
        }
    }

    sealed class CacheLoadType<out K, out V> {
        object CacheAside : CacheLoadType<Nothing, Nothing>()
        class ReadThrough<K, V>(val loader: (K) -> V) : CacheLoadType<K, V>()
    }

    companion object {
        inline fun <K, V> build(block: JustCacheBuilder<K, V>.() -> Unit) = JustCacheBuilder<K, V>().apply(block).build()

        inline fun <K, V> buildAsync(block: JustCacheBuilder<K, V>.() -> Unit) = JustCacheBuilder<K, V>().apply(block).buildAsync()
    }
}
