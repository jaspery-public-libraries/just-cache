package com.jaspery.kotlin.justcache.serviceloader

import java.util.ServiceLoader.load
import kotlin.reflect.KClass

internal fun <S : Any> serviceLoaderLoad(service: KClass<S>, cl: ClassLoader): Iterable<S> = load(service.java, cl)
