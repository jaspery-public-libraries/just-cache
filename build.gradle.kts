/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.jacoco.CodeCoverageUtils
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    val versions = object {
        val kotlin = "1.4.31"
    }
    kotlin("jvm") version versions.kotlin
    id("org.unbroken-dome.test-sets") version "2.2.1"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    jacoco
}

object Versions {
    // kotlin
    const val kotlin = "1.4.31"
    const val coroutines = "1.4.3"
}

buildscript {
    repositories {
        maven("https://jitpack.io")
    }
    dependencies {
        classpath("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jacoco-build-utils:-SNAPSHOT")
    }
}

allprojects {
    apply { plugin("kotlin") }
    apply { plugin("idea") }

    group = "com.jaspery.kotlin.justcache"
    version = "0.0.1-SNAPSHOT"

    repositories {
        maven("https://jitpack.io")
        mavenCentral()
    }
}

subprojects {
    apply { plugin("org.unbroken-dome.test-sets") }
    apply { plugin("io.spring.dependency-management") }
    apply { plugin("jacoco") }

    // Maven plugin is required for jitpack.io to work correctly
    apply { plugin("maven") }

    testSets {
        "integrationTest"()
    }

    dependencyManagement {
        dependencies {
            dependency("org.testng:testng:7.3.0")
            dependency("org.assertj:assertj-core:3.19.0")
            dependency("io.mockk:mockk:1.10.3")
            dependency("io.github.jupf.staticlog:staticlog:2.2.0")
            dependency("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}")
            dependency("org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}")
        }
    }

    dependencies {
        implementation(kotlin("stdlib-jdk8"))

        implementation("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jaspery-kotlin-lang:-SNAPSHOT")

        testImplementation("org.testng:testng")
        testImplementation("org.assertj:assertj-core")
        testImplementation("io.mockk:mockk")
        testImplementation("io.github.jupf.staticlog:staticlog")
        testImplementation("com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:kotlin-testng-dataprovider:-SNAPSHOT")
        testImplementation("org.jetbrains.kotlin:kotlin-reflect")

        add("integrationTestImplementation", sourceSets["test"].output)
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    tasks {
        val test by existing(Test::class) {
            useTestNG()
            reports {
                html.destination = file("$buildDir/reports/unit-tests/test")
            }
        }

        val integrationTest by existing(Test::class) {
            mustRunAfter(test)
            useTestNG()
            reports {
                html.destination = file("$buildDir/reports/int-tests/test")
            }
        }

        @Suppress("UNUSED_VARIABLE")
        val check by existing {
            dependsOn(integrationTest)
        }

        @Suppress("UnstableApiUsage")
        val jacocoUnitTestReport by registering(JacocoReport::class) {
            executionData(test.get())
            sourceSets(sourceSets.main)

            reports {
                xml.isEnabled = true
                csv.isEnabled = false
                html.isEnabled = true
            }
        }

        @Suppress("UnstableApiUsage")
        val jacocoIntegrationTestReport by existing(JacocoReport::class) {
            executionData(integrationTest.get())
            sourceSets(sourceSets.main)

            reports {
                xml.isEnabled = true
                csv.isEnabled = false
                html.isEnabled = true
            }
        }

        test.configure { finalizedBy(jacocoUnitTestReport) }
        integrationTest.configure { finalizedBy(jacocoIntegrationTestReport) }
    }
}

@Suppress("UsePropertyAccessSyntax", "UnstableApiUsage")
tasks.named<JacocoReport>("jacocoTestReport") {
    dependsOn(subprojects.map { it.tasks.test })

    executionData.setFrom(files(subprojects.map { it.tasks.jacocoUnitTestReport.executionData }).filter { f -> f.exists() })
    executionData.from(files(subprojects.map { it.tasks.jacocoIntegrationTestReport.executionData }).filter { f -> f.exists() })
    sourceDirectories.from(files(subprojects.flatMap { it.sourceSets.main.allSource.srcDirs }))
    classDirectories.from(files(subprojects.map { it.sourceSets.main.output }))

    reports {
        xml.isEnabled = true
        csv.isEnabled = true
        html.isEnabled = true
    }

    doLast {
        val xmlReport = this@named.reports.xml.destination
        val coverage = CodeCoverageUtils.extractCodeCoverageFromXmlReport(xmlReport)
        println("Total Coverage: ${Math.round(100 * coverage)}%")
    }
}

@Suppress("UnstableApiUsage")
val TaskContainer.jacocoUnitTestReport
    get() = this.named<JacocoReport>("jacocoUnitTestReport").get()

@Suppress("UnstableApiUsage")
val TaskContainer.jacocoIntegrationTestReport
    get() = this.named<JacocoReport>("jacocoIntegrationTestReport").get()

val TaskContainer.test
    get() = this.named("test")

val SourceSetContainer.main
    get() = this.named<SourceSet>("main").get()
